# EDI API
This is an API for translating EDI documents into either JSON or XML. It could be extended to do other things as well, including:
- perform logic on EDI files
- deposit files in a database and aggregate metrics
- convert JSON or XML into X12 EDI


Currently, it defines one endpoint under version 1: `POST /v1/convert`. This endpoint expects a POST body of:
```
{
  "api_key": <string>,
  "edi_document": <string>,
  "output_format": <string>
}
```
and returns either:
```
{
  "Ok": <string>
}
```
on success, or
```
{
  "Err": {
    "message": <string>
  }
}

```
on failure.

Build it with `docker build .`, or `cargo run`. The environment variable `RUST_LOG` can be used to determine logging level. Valid values are, from most detailed to least:
- `trace`
- `debug`
- `info`
- `warn`
- `error`

# Docs
Build the docs with `cargo doc`. Every function and struct in this code is documented.
