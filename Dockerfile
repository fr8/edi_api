# select build image
FROM rust:1.41 as build

# create a new empty shell project
RUN USER=root cargo new --bin edi_api
WORKDIR /edi_api

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src

# build for release
RUN rm ./target/release/deps/edi_api*
RUN cargo build --release

# our final base
FROM rust:1.23

# copy the build artifact from the build stage
COPY --from=build /edi_api/target/release/edi_api .

# Set the logging level.
ENV RUST_LOG=INFO

# set the startup command to run your binary
CMD ["./edi_api"]
