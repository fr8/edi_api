//! v1 of this API can be reached at `/v1`, and currently has one path:
//! `/convert`
//!
//! POST /v1/convert
//! body format:
//! {
//!    "api_key": string,
//!    "edi_document": string
//!    "output_format": string
//! }
//!
//! Returns either:
//! ```
//! {
//!     "Ok": <string>
//! }
//! ```
//! on success, or
//! ```
//! {
//!     "Err": {
//!         "message": <string>
//!     }
//! }
//! ```
//! on failure.
#![deny(missing_docs)]
#![deny(warnings)]
mod edi_conversion_error;
mod output_format;
use edi::parse;
use serde::{Deserialize, Serialize};
use warp::Filter;
use log::info;

use edi_conversion_error::EdiConversionError;
use output_format::OutputFormat;

/// The request type for an EDI conversion.
#[derive(Serialize, Deserialize)]
struct EdiConvertRequest {
    /// The API key for validation. Currently, this is not checked and is a TODO.
    api_key: String,
    /// The EDI document to be converted as a string.
    edi_document: String,
    /// The desired output format. Currently, this can be either "json" or "xml".
    output_format: String,
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let v1_api = warp::path("v1").and(
        warp::post()
            .and(warp::path("convert"))
            // Uncomment the below line to limit the content size of the body coming in.
            //        .and(warp::body::content_length_limit(1024 * 16))
            .and(warp::body::json())
            .map(|req: EdiConvertRequest| {
                warp::reply::json(&convert_edi_to_format(&req))
            })
            .with(warp::log::custom(|info| {
                info!("[{}] {} {:?} ({:?})", info.method(), info.path(), info.remote_addr(), info.user_agent());
            }))
    );
    warp::serve(v1_api).run(([127, 0, 0, 1], 3030)).await;
}

/// Performs the conversion using serde. Fails if either the output format is unsupported, the EDI
/// document is invalid, or the serialization fails (rare -- would be an internal server error).
fn convert_edi_to_format(req: &EdiConvertRequest) -> Result<String, EdiConversionError> {
    let output_format = OutputFormat::from_string(&req.output_format)?;
    let parsed = parse(&req.edi_document)?;

    Ok(match output_format {
        OutputFormat::Json => serde_json::to_string(&parsed)?,
        OutputFormat::Xml => quick_xml::se::to_string(&parsed)?,
    })
}
