use edi::EdiParseError;
use serde::{Deserialize, Serialize};
use std::fmt;
use log::{info, error};

/// An error that can occur in the execution of this API.
#[derive(Serialize, Deserialize)]
pub(crate) struct EdiConversionError {
    /// Details about the error.
    pub(crate) message: String,
}

impl fmt::Display for EdiConversionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error converting EDI: {}", self.message)
    }
}

impl fmt::Debug for EdiConversionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error converting EDI: {}", self.message)
    }
}

impl From<EdiParseError> for EdiConversionError {
    fn from(err: EdiParseError) -> Self {
        info!("Received malformed EDI input: {}", err);
        EdiConversionError {
            message: format!("{}", err),
        }
    }
}

impl From<quick_xml::DeError> for EdiConversionError {
    fn from(err: quick_xml::DeError) -> Self {
        error!("quick-xml error: {}", err);
        EdiConversionError {
            message: format!("Internal Error: {}", err),
        }
    }
}

impl From<serde_json::Error> for EdiConversionError {
    fn from(err: serde_json::Error) -> Self {
        error!("serde-json error: {}", err);
        EdiConversionError {
            message: format!("Internal Error: {}", err),
        }
    }
}
