use crate::EdiConversionError;
/// The potential output formats that this API supports. This is a configuration enum.
pub(crate) enum OutputFormat {
    Json,
    Xml,
}

impl OutputFormat {
    /// Convert a string into this enum. Fails if the string does not represent an enum variant.
    pub(crate) fn from_string(format: &str) -> Result<OutputFormat, EdiConversionError> {
        Ok(match format.to_lowercase().as_str() {
            "json" => OutputFormat::Json,
            "xml"  => OutputFormat::Xml,
             _     => return Err(EdiConversionError {
                 message: format!("Unsupported output_format: \"{}\". This API currently only supports JSON and XML output formats.", format)})
        })
    }
}
